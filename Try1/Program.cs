﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NeuralNetworkLibrary.Structure.Networks;
using NeuralNetworkLibrary.Data;
using System.IO;
using System.Diagnostics;
namespace Try1
{
    public class Abolone
    {
        public Abolone(double age, double sex, double PainType, double bloodPressure, double Serum, double bloodSugar, double restingElectrocardiographic,
            double maximumHeartRate, double inducedAngina, double oldpeakST, double peakST, double majorVessel, double thal)
        {
            Sex = sex;
            this.PainType = PainType;
            this.bloodPressure = bloodPressure;
            this.Serum = Serum;
            this.bloodSugar = bloodSugar;
            this.restingElectrocardiographic = restingElectrocardiographic;
            this.maximumHeartRate = maximumHeartRate;
            this.inducedAngina = inducedAngina;
            this.oldpeakST = oldpeakST;
            this.peakST = peakST;
            this.majorVessel = majorVessel;
            this.thal = thal;
            Age = age;
        }
        public double Age;
        public double Sex;
        public double PainType;
        public double bloodPressure;
        public double Serum;
        public double bloodSugar;
        public double restingElectrocardiographic;
        public double maximumHeartRate;
        public double inducedAngina;
        public double oldpeakST;
        public double peakST;
        public double majorVessel;
        public double thal;
    }
    class Program
    {
        public static List<Abolone> Parse(string pass)
        {
            List<Abolone> result = new List<Abolone>();
            using (StreamReader sr = new StreamReader(pass))
            {
                while (!sr.EndOfStream)
                {
                    string[] s = sr.ReadLine().Replace('.', ',').Split(' ');
                    result.Add(new Abolone(double.Parse(s[0]), double.Parse(s[1]), double.Parse(s[2]), double.Parse(s[3]), double.Parse(s[4]), double.Parse(s[5]),
                        double.Parse(s[6]), double.Parse(s[7]), double.Parse(s[8]), double.Parse(s[9]), double.Parse(s[10]), double.Parse(s[11]), double.Parse(s[12])));

                }
            }
            return result;
        }
        static void Main(string[] args)
        {
            List<DoubleData> train = new List<DoubleData>();
            List<DoubleData> check = new List<DoubleData>();
            using (StreamReader sr = new StreamReader(@"E:\Дима\FNN\NeuralNetworks\OboloneAge\bin\Debug\data.txt"))
            {
                while (!sr.EndOfStream)
                {
                    string[] s = sr.ReadLine().Replace('.', ',').Split(' ');
                    train.Add(new DoubleData(new double[] { double.Parse(s[0]), double.Parse(s[1]), double.Parse(s[2]), double.Parse(s[3]), double.Parse(s[4]), double.Parse(s[5]),
                        double.Parse(s[6]), double.Parse(s[7]), double.Parse(s[8]), double.Parse(s[9]), double.Parse(s[10]), double.Parse(s[11]),double.Parse(s[12]) }, new double[] { double.Parse(s[13]) }));

                }
            }
            MultiLayerNetwork MLN = new MultiLayerNetwork(new int[] { 25, 10, 5, 1 }, 13);
            DoubleDataList Data = new DoubleDataList(train);
            Data.Normalize();
            //double[] A = Data.Normalize();
            train = Data.DataList;
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            MLN.Train(train);
            stopWatch.Stop();
            Console.WriteLine(stopWatch.Elapsed);
            for (int i = 0; i < 20; i++)
            {
                check.Add(train[i]);
            }
            //MLN.Save("a.txt");

            double[][] coef = Data.Coef;

            for (int i = 0; i < check.Count; i++)
            {
                double result = (MLN.ComputeOutput(check[i].Input)[0] + 0.5) * coef[1][0];
                double output = (check[i].Output[0] + 0.5) * coef[1][0];
                /*if (Math.Abs(Out - 1) > Math.Abs(Out - 2))
                    Console.WriteLine("Заболевания сердца есть");
                else
                    Console.WriteLine("Заболеваний нет");*/
                string s = output.ToString() + "   " + result.ToString();
                Console.WriteLine(s);
            }
            Console.WriteLine();
        }
    }
}

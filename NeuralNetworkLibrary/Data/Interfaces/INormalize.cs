﻿namespace NeuralNetworkLibrary.Data.Interfaces
{
    public interface INormalize
    {
        void Normalize(double coef);
        void Restore();
    }
}

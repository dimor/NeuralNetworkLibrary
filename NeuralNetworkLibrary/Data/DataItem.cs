﻿namespace NeuralNetworkLibrary.Data
{
    public class DataItem<T>
    {
        protected T[] input = null;
        protected T[] output = null;

        public DataItem() { }

        public DataItem(T[] Input, T[] Output)
        {
            input = Input;
            output = Output;
        }

        public T[] Input
        {
            get { return input; }
            set { input = value; }
        }

        public T[] Output
        {
            get { return output; }
            set { output = value; }
        }
    }
}

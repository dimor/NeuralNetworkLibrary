﻿namespace NeuralNetworkLibrary.Data
{
    public class DoubleData : DataItem<double>
    {
        public DoubleData() : base() { }
        public DoubleData(double[] Input, double[] Output) : base(Input, Output) { }


        public void Normalize(double[][] Coef)
        {
            double[] coef = Coef[0];
            for (int i = 0; i < input.Length; i++)
            {
                input[i] /= coef[i];
                input[i] -= 0.5;
            }
            coef = Coef[1];
            for (int i = 0; i < output.Length; i++)
            {
                output[i] /= coef[i];
                output[i] -= 0.5;
            }
        }
        public void DeNormalize(double[][] Coef)
        {
            double[] coef = Coef[0];
            for (int i = 0; i < input.Length; i++)
            {
                input[i] += 0.5;
                input[i] *= coef[i];
            }
            coef = Coef[1];
            for (int i = 0, j = input.Length; i < output.Length; i++, j++)
            {
                output[i] += 0.5;
                output[i] *= coef[j];

            }
        }
    }
}

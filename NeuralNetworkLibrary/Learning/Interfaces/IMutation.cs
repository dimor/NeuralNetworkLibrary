﻿using NeuralNetworkLibrary.Structure.Components.Interfaces;

namespace NeuralNetworkLibrary.Learning.Interfaces
{
    interface IMutation
    {
        void mutate(INeuron Neuron, int k);
    }
}

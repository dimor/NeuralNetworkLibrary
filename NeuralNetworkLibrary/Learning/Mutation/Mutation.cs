﻿using System;
using NeuralNetworkLibrary.Learning.Interfaces;
using NeuralNetworkLibrary.Structure.Components.Interfaces;

namespace NeuralNetworkLibrary.Learning.Mutation
{
    internal class Mutation : IMutation
    {
        Random _rnd;
        internal Mutation(Random rnd, double distance)
        {
            _rnd = rnd;
        }
        public void mutate(INeuron neuron, int k)
        {
            double rate = _rnd.NextDouble() - 0.333;
            neuron.Weights[k] *= rate;

        }
    }
}

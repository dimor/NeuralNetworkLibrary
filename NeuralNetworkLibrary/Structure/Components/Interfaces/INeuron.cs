﻿using System.Collections.Generic;
using NeuralNetworkLibrary.Structure.Components.Interfaces;

namespace NeuralNetworkLibrary.Structure.Components.Interfaces
{
    public interface INeuron
    {
        /// <summary>
        /// Weights of the neuron
        /// </summary>
        List<double> Weights { get; }

        /// <summary>
        /// Offset/bias of neuron (default is 0)
        /// </summary>
        double Bias { get; set; }
    
        /// <summary>
        /// Last calculated state in Activate
        /// </summary>
        double LastState { get; set; }

        /// <summary>
        /// Last calculated NET in NET
        /// </summary>
        double WeightedSum { get; set; }

        IList<INeuron> Children { get; }

        IList<INeuron> Parents { get; }

        IFunction ActivationFunction { get; set; }

        double ActivationFunctionDerivative { get; }

        double ErrorDerivative { get; set; }

        /// <summary>
        /// Compute NET of the neuron by input vector
        /// </summary>
        /// <param name="inputVector">input vector (must be the same dimension as was set in SetDimension)</param>
        /// <returns>NET of neuron</returns>
        double computeWeightedSum(double[] inputVector);
        /// <summary>
        /// Compute state of neuron
        /// </summary>
        /// <param name="inputVector">input vector (must be the same dimension as was set in SetDimension)</param>
        /// <returns>State of neuron</returns>
        double Activate(double[] inputVector);
    }
}

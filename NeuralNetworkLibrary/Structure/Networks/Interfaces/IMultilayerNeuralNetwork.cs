﻿using NeuralNetworkLibrary.Structure.Components.Interfaces;
using System.Collections.Generic;

namespace NeuralNetworkLibrary.Structure.Networks.Interfaces
{
    public interface IMultilayerNeuralNetwork : INeuralNetwork
    {
        /// <summary>
        /// Get array of layers of network
        /// </summary>
        ILayer[] Layers { get; }
    }
}

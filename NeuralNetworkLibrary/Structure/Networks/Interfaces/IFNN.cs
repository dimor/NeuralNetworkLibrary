﻿using System.Collections.Generic;
using NeuralNetworkLibrary.Data;
using NeuralNetworkLibrary.Structure.Components.Interfaces;
using System.IO;

namespace NeuralNetworkLibrary.Structure.Networks.Interfaces
{
    public interface IFNN
    {
        /// <summary>
        /// Compute output vector by input vector
        /// </summary>
        /// <param name="inputVector">input vector (double[])</param>
        /// <returns>Output vector (double[])</returns>
        FuzzyData[] ComputeOutput(FuzzyData[] inputVector);

        //Stream Save();
        void Load(Stream S);
        /// <summary>
        /// Train network with given inputs and outputs
        /// </summary>
        /// <param name="inputs">Set of input vectors</param>
        /// <param name="outputs">Set if output vectors</param>
        void Train(IList<DataItem<FuzzyData>> data);

        /// <summary>
        /// Get array of layers of network
        /// </summary>
        ILayer[] Layers { get; }
    }
}

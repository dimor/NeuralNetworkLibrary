﻿using System;
using System.Collections.Generic;
using NeuralNetworkLibrary.Structure.Components.Interfaces;
using NeuralNetworkLibrary.Structure.Networks.Interfaces;
using NeuralNetworkLibrary.Structure.Components;
using NeuralNetworkLibrary.Data;
using NeuralNetworkLibrary.Learning;
using System.IO;
using System.Linq;

namespace NeuralNetworkLibrary.Structure.Networks
{
    static class Extensions
    {
        public static List<T> Clone<T>(this List<T> listToClone) where T : ICloneable
        {
            return listToClone.Select(item => (T)item.Clone()).ToList();
        }
    }
    public class MultiLayerNetwork:IMultilayerNeuralNetwork
    {
        Layer[] layers;
        BackpropagationMLNLearningAlgorithm learnStrategy;


        public MultiLayerNetwork(int[] NeuronsInLayers, int InputDimension)
        {
            Random rnd = new Random();
            layers = new Layer[NeuronsInLayers.Length];
            layers[0] = new Layer(NeuronsInLayers[0], InputDimension);
            double[] weights = new double[InputDimension];
            for (int i = 0; i < InputDimension; i++)
            {
                weights[i] = 1;
            }
            for (int i = 0; i < NeuronsInLayers[0]; i++)
            {
                layers[0].AddNeuron(new MLNeuron(weights.ToList()));
            }
            for (int i = 1; i < NeuronsInLayers.Length; i++)
            {
                layers[i] = new Layer(NeuronsInLayers[i], NeuronsInLayers[i - 1]);
                for (int j = 0; j < NeuronsInLayers[i]; j++)
                    layers[i].AddNeuron(new MLNeuron(NeuronsInLayers[i - 1], rnd));
            }
            LearningConfig config = new LearningConfig();
            config.BatchSize = 10;
            config.MaxEpoches = 10000;
            config.MinError = 0.0001;
            config.MinErrorChange = 0.000000001;
            learnStrategy = new BackpropagationMLNLearningAlgorithm(this, config);
        }


        public ILayer[] Layers
        {
            get { return layers; }
        }


        public double[] ComputeOutput(double[] inputVector)
        {
            double[] outputVector = layers[0].Compute(inputVector);
            for (int i = 1; i < layers.Length; i++)
            {
                outputVector = layers[i].Compute(outputVector);
            }
            return outputVector;
        }

        public void Train(IList<DoubleData> Data)
        {
            learnStrategy.train(Data);
        }

        public void Save(string Path)
        {
            StreamWriter f = new StreamWriter(Path);
            f.WriteLine(layers.Length);
            foreach (Layer layer in layers)
            {
                f.WriteLine(layer.Neurons.Length);
                f.WriteLine(layer.InputDimension);
                foreach (INeuron neuron in layer.Neurons)
                {
                    foreach (double a in neuron.Weights)
                        f.Write(a + " ");
                    f.Write(";");
                }
                f.WriteLine();
            }
            f.Close();
        }

        public void Load(Stream S)
        {
            StreamReader sr = new StreamReader(S);
            string s;
            int i = 0;
            s = sr.ReadLine();
            layers = new Layer[int.Parse(s)];
            while (!sr.EndOfStream)
            {
                s = sr.ReadLine();
                int n;
                int m;
                if (int.TryParse(s, out n))
                {
                    int.TryParse(sr.ReadLine(), out m);
                    layers[i] = new Layer(n, m);
                }
                else
                {
                    string[] st = s.Split(';');
                    List<double> weigth = new List<double>();
                    foreach(string line in st)
                    {
                        weigth.Add(double.Parse(line));
                    }
                    layers[i].AddNeuron(new MLNeuron(weigth));
                }
            }
        }       
    }
}
